/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.opcode;

/**
 * @author Steven
 *
 */
public enum Send {
	
	UNKNOWN(0x0),
	ACCOUNT(0x1),
	BODY(0x02),
	GAME_STATE(0x3),
	INPUT(0x4);
	
	
	private int code;
	
	private Send(int code) {
		this.code = code;
	}
	
	public int getId() {
		return code;
	}
	
	public Send getRecById(int code) {
		for (Send r : Send.values()) {
			if (r.code == code) {
				return r;
			}
		}
		return null;
	}
	
}
