/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.opcode;

import io.netty.buffer.ByteBuf;
import net.brokegames.logic.LocalClient;

/**
 * @author Steven
 *
 */
public class AbstractPacketHandler implements PacketHandler {

	/** (non-Javadoc)
	 * @see net.brokegames.server.opcode.PacketHandler#handlePacket(net.brokegames.logic.DummyClient, io.netty.buffer.ByteBuf)
	 */
	@Override
	public void handlePacket(ByteBuf data) {}

	/** (non-Javadoc)
	 * @see net.brokegames.server.opcode.PacketHandler#isLoggedIn(net.brokegames.logic.DummyClient)
	 */
	@Override
	public boolean isLoggedIn() {
		return LocalClient.isLoggedIn(); 
	}

}
