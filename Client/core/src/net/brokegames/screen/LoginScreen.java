package net.brokegames.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import net.brokegames.client.AppMain;
import net.brokegames.handlers.outbound.PacketCreator;
import net.brokegames.logic.ClientConstants;
import net.brokegames.logic.LocalClient;

/**
 * Created by Steven on 1/12/2018.
 */

public class LoginScreen implements Screen {

    private Texture texture;
    private OrthographicCamera camera;
    private TextField username;
    private TextField password;
    private Stage stage;
    private Skin skin;
    private ImageButton createAccount;
    private ImageButton login;
    private AppMain game;

    public LoginScreen(AppMain game) {
        this.game = game;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera));
        texture = new Texture(Gdx.files.internal("core/assets/loginscreen.png"));
        skin = new Skin(Gdx.files.internal("core/assets/skins/gdx-holo/skin/uiskin.json"));
        username  = new TextField("", skin);
        username.setText("Email");
        username.setMaxLength(255);
        username.setSize(450, 40);
        username.setPosition(89, ClientConstants.HEIGHT - 589);
        stage.addActor(username);


        password  = new TextField("", skin);
        password.setPasswordMode(true);
        password.setText("Password");
        password.setMaxLength(32);
        password.setSize(450, 40);
        password.setPosition(89, ClientConstants.HEIGHT - 701);
        stage.addActor(password);

        Drawable drawCreateAccount = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("core/assets/createAccountButton.png"))));
        createAccount = new ImageButton(drawCreateAccount);
        createAccount.setPosition(57, ClientConstants.HEIGHT - 920);
        createAccount.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                return true;
            }
        });
        stage.addActor(createAccount);

        Drawable drawLogin = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("core/assets/loginbutton.png"))));
        login = new ImageButton(drawLogin);
        login.setPosition(57, ClientConstants.HEIGHT - 1100);

        login.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                LocalClient.write(PacketCreator.sendUsernamePassword(0, username.getText(), password.getText()));
                dispose();
                game.setScreen(new GameScreen(game));
                return true;
            }
        });
        stage.addActor(login);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        stage.getCamera().update();
        stage.getBatch().begin();
        stage.getBatch().draw(texture, 0, 0);
        stage.getBatch().end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        texture.dispose();
        stage.dispose();
    }
}
