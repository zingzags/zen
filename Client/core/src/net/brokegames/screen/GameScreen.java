package net.brokegames.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import net.brokegames.client.AppMain;
import net.brokegames.handlers.outbound.PacketCreator;
import net.brokegames.logic.ClientConstants;
import net.brokegames.logic.LocalClient;
import net.brokegames.world.GameWorld;

/**
 * Created by Steven on 1/17/2018.
 */

public class GameScreen implements Screen {

    private OrthographicCamera camera;
    private ExtendViewport viewport;
    private AppMain main;

    public GameScreen(AppMain main) {
        this.main = main;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		viewport = new ExtendViewport(ClientConstants.WIDTH, ClientConstants.HEIGHT, camera);
		viewport.apply();
    }

    @Override
    public void show() {
        // Create our body definition
        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.position.set(new Vector2(0, 15));
        Body groundBody = GameWorld.getPhysics().createBody(groundBodyDef);
        PolygonShape groundBox = new PolygonShape();
        groundBox.setAsBox(Gdx.graphics.getWidth(), .5f);
        groundBody.createFixture(groundBox, 0.0f);
        groundBox.dispose();
        //LocalClient.write(PacketCreator.sendBodyRequest());
    }

    @Override
    public void render(float delta) {
        main.game.update(camera);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
