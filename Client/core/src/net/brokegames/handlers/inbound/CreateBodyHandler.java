package net.brokegames.handlers.inbound;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import io.netty.buffer.ByteBuf;
import net.brokegames.client.AppMain;
import net.brokegames.components.FieldObject;
import net.brokegames.components.Player;
import net.brokegames.logic.LocalClient;
import net.brokegames.opcode.AbstractPacketHandler;
import net.brokegames.server.physics.CollisionFilter;
import net.brokegames.systems.InputMovementSystem;
import net.brokegames.world.GameWorld;

public class CreateBodyHandler extends AbstractPacketHandler {

	/**
	 * The purpose of this class is to create the physics body that will
	 * be associated to the player.
	 */
	@Override
	public void handlePacket(ByteBuf data) {
		int fieldId = data.readInt();
		Vector2 position = new Vector2(data.readFloat(), data.readFloat());
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.fixedRotation = true;
        bodyDef.position.set(position);
        bodyDef.allowSleep = false;
        Body body = GameWorld.getPhysics().createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(10, 10);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;
        fixtureDef.friction = .5f;
        fixtureDef.filter.categoryBits = CollisionFilter.PLAYER_FILTER;
        fixtureDef.filter.maskBits = CollisionFilter.MAP_BOUNDS_FILTER;
        body.createFixture(fixtureDef);
        shape.dispose();
        Entity entity = new Entity();
        entity.add(new Player(body));
        entity.add(new FieldObject(fieldId));
		AppMain.game.getEngine().addEntity(entity);
        if (LocalClient.getEntity() == null) {
        	LocalClient.setEntity(entity);
        }
	}
	
}
