package net.brokegames.handlers.outbound;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import net.brokegames.opcode.Send;

public class PacketCreator {

    private static void writeAsciiString(ByteBuf buffer, String s) {
        buffer.writeShort(s.length());
        buffer.writeBytes(s.getBytes());
    }

    public static String readAsciiString(ByteBuf buffer){
        int size = buffer.readByte();
        char[] string = new char[size];
        for (int i = 0; i < size; i++) {
            string[i] = (char) buffer.readShort();
        }
        return String.valueOf(string);
    }

    public static ByteBuf sendUsernamePassword(int loginType, String email, String password) {
        ByteBuf packet = Unpooled.buffer();
        packet.writeShort(Send.ACCOUNT.getId());
        packet.writeInt(loginType);
        writeAsciiString(packet, email);
        writeAsciiString(packet, password);
        return packet;
    }

    public static ByteBuf sendBodyRequest() {
        ByteBuf packet = Unpooled.buffer();
        packet.writeShort(Send.BODY.getId());
        return packet;
	}



}
