/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.handlers.player.io;

/**
 * @author Steven
 *
 */
public enum Direction {
	
	UP(0),
	DOWN(1),
	LEFT(2),
	RIGHT(3);
	
	private int direction;
	
	private Direction(int direction) {
		this.direction = direction;
	}
	
	public int getDirection() {
		return direction;
	}
	
	
}
