package net.brokegames.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;

import net.brokegames.player.Gender;

public class Player implements Component {

	private int id;
	private String name;
	//private Client client;
	private Body body;
	private Gender gender;

	public Player() {}
	
	public Player(Body body) {
		this.body = body;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the client
	 */
	/*public Client getClient() {
		return client;
	}*/
	
	/**
	 * @param client the client to set
	 */
	/*public void setClient(Client client) {
		this.client = client;
	} */
	
	/**
     * @return the body
     */
    public Body getBody() {
      return body;
    }
  
    /**
     *@param body the physics body object
     */
    public void setBody(Body body) {
      this.body = body;
    }
    
    /**
     * Removes the buddy reference 
     */
    public void removeBody() {
    	this.body = null;
    }

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * Sets the gener of the player
	 * @param gender
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
}
