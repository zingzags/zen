/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.logic;

import com.badlogic.gdx.math.Vector2;

/**
 * @author Steven
 *
 */
public class ClientConstants {

	public static final int WIDTH = 640;
	public static final int HEIGHT = 1136;
	public static final boolean FULLSCREEN = false;
	public static final String TITLE = "Project Zen";
	public static final boolean DEBUG = true;
	public static final Vector2 GRAVITY = new Vector2(0, -9.8f);
}
