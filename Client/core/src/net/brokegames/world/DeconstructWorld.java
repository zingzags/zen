/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.world;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

/**
 * @author Steven
 *
 */
public class DeconstructWorld {
  
  /**
   * This class handles the rips apart the game world,
   * and only sends what is needed to the server.
   * This class is mainly for movement and physics. 
   *
   */
  public DeconstructWorld() {
    Array<Body> bodies = new Array<Body>();
    GameWorld.getPhysics().getBodies(bodies);
  }
  
  
}
