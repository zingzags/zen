/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.world;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import net.brokegames.client.AppMain;
import net.brokegames.logic.ClientConstants;
import net.brokegames.systems.InputMovementSystem;

/**
 * @author Steven
 *
 */
public class GameWorld {

	private static World world;
	private Box2DDebugRenderer debug;
	private PooledEngine engine = new PooledEngine();

	/**
	 * This class will create the {@link GameWorld} 
	 * It will handle the physics for the client
	 */
	public GameWorld() {
		Gdx.app.log("LOADING_GAMEWORLD", "Initializing game world.");
		world  = new World(ClientConstants.GRAVITY, true);
		//engine.addSystem(new InputMovementSystem());

	}
	
	/**
	 * This method will update the physics 
	 * 
	 */
	public void update(Camera camera) {
		world.step(1/60f, 8, 3);
		if (ClientConstants.DEBUG && debug == null) {
			debug = new Box2DDebugRenderer();
		}
		if (debug != null) {
			debug.render(world, camera.combined);
		}
		engine.update(Gdx.graphics.getDeltaTime());
	}
	
	/**
	 * This method will return the {@link World} physics instance 
	 * @return {@link World}
	 */
	public static World getPhysics() {
		return world;
	}
	
	/**
	 * This method will return the {@link Engine} instance 
	 * @return {@link Engine}
	 */
	public Engine getEngine() {
		return engine;
	}

}