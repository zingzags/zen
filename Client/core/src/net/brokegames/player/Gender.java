/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.player;

/**
 * @author Steven
 *
 */
public enum Gender {
	UNKOWN(-1),
	MALE(0),
	FEMALE(1);
	
	private int gender;
	
	private Gender(int gender) {
		this.gender = gender;
	}

	/**
	 * @return the gender
	 */
	public int getGender() {
		return gender;
	}
}
