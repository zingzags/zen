package net.brokegames.client;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;

import net.brokegames.logic.ClientConstants;
import net.brokegames.network.ClientInitializer;
import net.brokegames.screen.LoginScreen;
import net.brokegames.world.GameWorld;

public class AppMain extends Game {
	
	private ClientInitializer client;
    public static GameWorld game;

	@Override
	public void create() {
		Gdx.app.setLogLevel(Application.LOG_INFO);
        game = new GameWorld();
		client = new ClientInitializer("127.0.0.1", 8484);
		client.start();
        setScreen(new LoginScreen(this));
	}

	@Override
	public void render() {
		Gdx.graphics.setTitle(ClientConstants.TITLE);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glEnable(GL30.GL_BLEND);
		super.render();
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		//camera.position.set(0, 0, 0); //TODO Set this to relative to the players position when resizing the screen
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#pause()
	 */
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#resume()
	 */
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#dispose()
	 */
	@Override
	public void dispose() {
		//client.close();
	}
}
