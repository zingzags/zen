package net.brokegames.server;

import com.badlogic.gdx.Gdx;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Steven on 1/10/2018.
 */

public class ServerConstants {

    public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    public static ServerConfig config = gson.fromJson(Gdx.files.internal("configuration/config.json").reader(), ServerConfig.class);

    public static void loadConfigurations() {
        reloadServerConfig();
    }

    public static void reloadServerConfig() {
        config = gson.fromJson(Gdx.files.internal("configuration/config.json").reader(), ServerConfig.class);
    }


}
