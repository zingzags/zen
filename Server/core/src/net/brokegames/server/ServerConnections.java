package net.brokegames.server;

/**
 * Created by Steven on 1/10/2018.
 */

public class ServerConnections {

    private String ip;
    private int port;
    private int maximumConnections;
    private ServerType type;

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public int getMaximumConnections() {
        return maximumConnections;
    }

    public ServerType getType() {
        return type;
    }
}
