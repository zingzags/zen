/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import net.brokegames.server.component.Mapper;
import net.brokegames.server.component.Player;

/**
 * @author Steven
 *
 */
public class Client {

	public static final AttributeKey<Client> CLIENT_KEY = AttributeKey.valueOf("CLIENT");
	private Channel connection;
	private long sessionId;
	private int world;
	private int channel;
	private Entity entity;

	public Client(Channel connection) {
		this.connection = connection;
	}

	/**
	 * @return Gets the sessionId of the specified client
	 */
	public long getSessionId() {
		return sessionId;
	}

	/**
	 */
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	
	/**
	 */	
	public synchronized void write(ByteBuf packet) {
		connection.writeAndFlush(packet);
	}

	/**
	 * @return the channel index
	 */
	public int getChannel() {
		return channel;
	}

	/**
	 */
	public void setChannel(int channel) {
		this.channel = channel;
	}

	/**
	 * @return the world index
	 */
	public int getWorld() {
		return world;
	}

	/**
	 */
	public void setWorld(int world) {
		this.world = world;
	}

	/**
	 * @return the connection
	 */
	public Channel getConnection() {
		return connection;
	}
	
	/** 
	 * Disconnects the session 
	 */
	public void disconnect() {
		connection.disconnect();
	}
	
	public boolean isLoggedIn() {
		return true;
	}

	/**
	 * @return the player entity
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 */
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	/**
	 * @return the Player object 
	 */
	public Player getPlayer() {
	    return Mapper.pObj.get(entity);
	}
	
	/**
	 * This method will remove the body object
	 * when the session disconnects 
	 */
	public void remove() {
		Body body = getPlayer().getBody();
		Server.master.getWorld().destroyBody(body);
		getPlayer().removeBody();
		Server.master.getEngine().removeEntity(entity);
	}
}
