/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.opcode;

/**
 * @author Steven
 *
 */
public enum Receive {
	
	UNKNOWN(0x0),
	ACCOUNT(0x01),
	BODY(0x02),
	GAME_STATE(0x03),
	INPUT(0x04);
	
	
	private int code;
	
	private Receive(int code) {
		this.code = code;
	}
	
	public Receive getRecById(int code) {
		for (Receive r : Receive.values()) {
			if (r.code == code) {
				return r;
			}
		}
		return null;
	}
	
}
