/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.opcode;

/**
 * @author Steven
 *
 */
public enum Send {
	
	UNKNOWN(0x0),
	ACCOUNT(0x01),
	BODY(0x02);
	
	private int code;
	
	private Send(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
	
	public Send getRecById(int code) {
		for (Send r : Send.values()) {
			if (r.code == code) {
				return r;
			}
		}
		return null;
	}
	
}
