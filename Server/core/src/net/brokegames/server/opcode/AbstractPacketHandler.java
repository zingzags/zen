/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.opcode;

import io.netty.buffer.ByteBuf;
import net.brokegames.server.Client;

/**
 * @author Steven
 *
 */
public class AbstractPacketHandler implements ReceivePacketHandler {

	/** (non-Javadoc)
     */
	@Override
	public void handlePacket(Client c, ByteBuf data) {}

	/** (non-Javadoc)
     */
	@Override
	public boolean isLoggedIn(Client c) {
		return c.isLoggedIn(); 
	}

}
