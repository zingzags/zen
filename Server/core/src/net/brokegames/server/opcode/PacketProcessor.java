/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.opcode;

import java.util.LinkedHashMap;
import java.util.Map;

import com.badlogic.gdx.utils.Array;

import net.brokegames.server.handlers.inbound.AccountHandler;
import net.brokegames.server.handlers.inbound.AddBodyHandler;
import net.brokegames.server.handlers.inbound.InputHandler;

/**
 * @author Steven
 *
 */
public class PacketProcessor {

	private final static Map<String, PacketProcessor> instances = new LinkedHashMap<String, PacketProcessor>();
	private Array<Packet> packets;
	
	public PacketProcessor() {
		packets = new Array<Packet>();
	}
	
	public ReceivePacketHandler getHandler(short code) {
		for (Packet p : packets) {
			if (p.code == Receive.UNKNOWN.getRecById(code)) {
				return p.handler;
			}
		}
		return null;
	}
	
	public static PacketProcessor getProcessor(int world, int channel) {
		PacketProcessor processor = instances.get(world + " " + channel);
		if (processor == null) {
			processor = new PacketProcessor();
			processor.reset(channel);
			instances.put(world + " " + channel, processor);
		}
		return processor;
	}
	
	public void reset(int channel) {
		packets.clear();
		packets.add(new Packet(Receive.ACCOUNT, new AccountHandler()));
		packets.add(new Packet(Receive.BODY, new AddBodyHandler()));
		packets.add(new Packet(Receive.INPUT, new InputHandler()));
	}
	
	public class Packet {
		
		public Receive code;
		public ReceivePacketHandler handler;
		
		public Packet(Receive code, ReceivePacketHandler handler) {
			this.code = code;
			this.handler = handler;
		}	
	}
	
}
