package net.brokegames.server;

public enum ConnectionFlag {
	
	UNKOWN(-1),
	LOGGED_OUT(0),
	LOGGEDIN(1),
	SERVERLIST(2),
	IN_GAME(3);
	
	private int flag;
	
	private ConnectionFlag(int flag) {
		this.flag = flag;
	}
	
	public int getFlag() {
		return flag;
	}
	
}
