package net.brokegames.server.handlers.inbound;

import net.brokegames.server.Client;
import net.brokegames.server.handlers.outbound.PacketCreator;
import net.brokegames.server.opcode.AbstractPacketHandler;

import io.netty.buffer.ByteBuf;

/**
 * Created by Steven on 1/17/2018.
 */

public class AccountHandler extends AbstractPacketHandler {

    @Override
    public void handlePacket(Client c, ByteBuf data) {
        int accountType = data.readInt(); //Different account login types
        String email = PacketCreator.readAsciiString(data);
        String password = PacketCreator.readAsciiString(data);
    }

}
