/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.handlers.inbound;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import io.netty.buffer.ByteBuf;
import net.brokegames.server.Client;
import net.brokegames.server.Server;
import net.brokegames.server.component.Player;
import net.brokegames.server.handlers.outbound.PacketCreator;
import net.brokegames.server.opcode.AbstractPacketHandler;
import net.brokegames.server.physics.CollisionFilter;
import net.brokegames.www.ProjectZen;

/**
 * @author Steven
 *
 */
public class AddBodyHandler extends AbstractPacketHandler {
  
	@Override
	public void handlePacket(Client c, ByteBuf data) {
        Body body;
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.fixedRotation = true;
        bodyDef.position.set(100, 60);
        bodyDef.allowSleep = false;
        body = Server.master.getWorld().createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(10, 10);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;
        fixtureDef.friction = .5f;
        fixtureDef.filter.categoryBits = CollisionFilter.PLAYER_FILTER;
        fixtureDef.filter.maskBits = CollisionFilter.MAP_BOUNDS_FILTER;
        body.createFixture(fixtureDef);
        shape.dispose();
        Entity entity = new Entity();
        entity.add(new Player(body));
        ProjectZen.field.addFieldObject(entity);
        Server.master.getEngine().addEntity(entity);
        c.setEntity(entity);
        c.write(PacketCreator.createBodyResponse(entity));
	}
}
