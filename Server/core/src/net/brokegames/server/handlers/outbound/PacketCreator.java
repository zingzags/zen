package net.brokegames.server.handlers.outbound;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.brokegames.server.component.FieldObject;
import net.brokegames.server.component.Player;
import net.brokegames.server.opcode.Send;

public class PacketCreator {

	private static void writeAsciiString(ByteBuf buffer, String s) {
		buffer.writeShort(s.length());
		buffer.writeBytes(s.getBytes());
	}

    public static String readAsciiString(ByteBuf buffer){
        int size = buffer.readShort();
        char[] string = new char[size];
        for (int i = 0; i < size; i++) {
            string[i] = (char) buffer.readByte();
        }
        return String.valueOf(string);
    }

    public static ByteBuf accountVerifictionResponse(int response) {
        ByteBuf packet = Unpooled.buffer();
        packet.writeShort(Send.ACCOUNT.getCode());
        packet.writeByte(response);
        return packet;
    }

	public static ByteBuf createBodyResponse(Entity entity) {
		ByteBuf packet = Unpooled.buffer();
		packet.writeShort(Send.BODY.getCode());
		Player player = entity.getComponent(Player.class);
		Vector2 position = player.getBody().getPosition();
		packet.writeInt(entity.getComponent(FieldObject.class).getFieldId());
		packet.writeFloat(position.x);
		packet.writeFloat(position.y);
		return packet;
	}
	
	
}
