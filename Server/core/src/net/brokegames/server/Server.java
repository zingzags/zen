/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import net.brokegames.world.WorldFactory;

/**
 * @author Steven
 *
 */
public class Server {

	public static Server master = new Server();
	private PooledEngine engine = new PooledEngine();
	private World world = new World(new Vector2(ServerConstants.config.getGravityX(), ServerConstants.config.getGravityY()), true);
	private Box2DDebugRenderer debug;
	private WorldFactory worlds = new WorldFactory();
	
	/**
	 * This is the main server class, this class will handle
	 * the physics engine, the entity framework, and the
	 * {@link World} instances 
	 */
	public Server() {}
	
	/**
	 * This is the update method used in the main game loop,
	 * It will handle:
	 * {@link Box2DDebugRenderer} - The physics body debug renderer 
	 * {@link World} - The physics engine
	 * {@link Engine} - The entity framework
	 * @param {@link Camera} - The game camera
	 */
	public void update(Camera camera) {
		engine.update(Gdx.graphics.getDeltaTime());
		world.step(1/60f, 8, 3);
		if (ServerConstants.config.isDebug() && debug == null) {
			 debug = new Box2DDebugRenderer();
		}
		if (debug != null) {
			debug.render(world, camera.combined);
		}
	}
	
	/**
	 * This method will return the {@link WorldFactory} instance 
	 * @return {@link WorldFactory}
	 */
	public WorldFactory getWorldFactory() {
		return worlds;
	}
	
	/**
	 * This method will return the {@link World} instance 
	 * @return {@link World}
	 */
	public World getWorld() {
		return world;
	}
	/**
	 * This method will dispose:
	 * {@link World} - Which handles all the physics
	 * 
	 */
	public void dispose() {
		world.dispose();
	}
	
	/**
	 * This method will return the {@link Engine} instance 
	 * @return {@link Engine}
	 */
	public Engine getEngine() {
		return engine;
	}
	
}
