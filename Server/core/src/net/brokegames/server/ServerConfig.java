package net.brokegames.server;

/**
 * Created by Steven on 1/10/2018.
 */

public class ServerConfig {

    private String title;
    private boolean debugMode;
    private float gravityX;
    private float gravityY;
    private ServerConnections[] serverConnections;

    public String getTitle() {
        return title;
    }

    public boolean isDebug() {
        return debugMode;
    }

    public float getGravityX() {
        return gravityX;
    }

    public float getGravityY() {
        return gravityY;
    }

    public ServerConnections[] getConnections() {
        return serverConnections;
    }

}
