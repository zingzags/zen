/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.network;

import com.badlogic.gdx.Gdx;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author Steven
 *
 */
public class ServerInitializer {

	private ServerBootstrap bootStrapper;
	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;
	private Channel chan;	
	private int port;
	private int maximumConnections;
	private int world = -1;
	private int channel = -1;
	
	/**
	 * This constructor specifies the port number which the server should bind to, and also specifies the maximum number of connections allowed to the server
	 * @param port - The port number
	 * @param maximumConnections - The maximum number of connections allowed with this specified port
	 */
	public ServerInitializer(int port, int maximumConnections) {
		this.port = port;
		this.maximumConnections = maximumConnections;
		bossGroup = new NioEventLoopGroup(1);
		workerGroup  = new NioEventLoopGroup();
		bootStrapper = new ServerBootstrap();
		bootStrapper.group(bossGroup, workerGroup)
		.channel(NioServerSocketChannel.class)
		.handler(new LoggingHandler(LogLevel.DEBUG))
		.option(ChannelOption.SO_BACKLOG, maximumConnections)
		.childOption(ChannelOption.TCP_NODELAY, true)
		.childOption(ChannelOption.SO_KEEPALIVE, true);
	}
	
	/**
	 * Starts binding to the specified port
	 * @return boolean - Depending if the port has binded successfully
	 */
	public boolean start() {
		try {
			bootStrapper.childHandler(new TrafficHandler(world, channel));
			chan = bootStrapper.bind(port).sync().channel().closeFuture().channel();
		} catch (Exception e) {
			Gdx.app.error("BINDING_ISSUE", "There was an error trying to bind to the port", e);
			return false;
		} 
		Gdx.app.log("PORT_BOUND", "Listening to port: " + port);
		return true;
	}
	
	/**
	 * Adds another childHandler to the server.
	 */
	public void addHandler(ChannelHandler childHandler) {
		bootStrapper.childHandler(childHandler);
	}
	
	/**
	 * Closes down the connection
	 */
	public void close () {
		try {
			chan.close();
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
			Gdx.app.log("SHUTDOWN_SUCCESS", "The server has been shut down.");
		} catch(Exception e) {
			Gdx.app.error("SHUTDOWN_ERROR", "There has been an issue trying to shut the server down.\n", e);
		}
	}


	/**
	 * This method returns the port number
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	
	/**
	 * This sets the world and the channel that is bound to this port
	 */
	public void setWorldAndChannel(int world, int channel) {
		this.world = world;
		this.channel = channel;
	}

	/**
	 * This method returns the maximum number of connections allowed
	 * @return the maximumConnections
	 */
	public int getMaximumConnections() {
		return maximumConnections;
	}	

	
	
}
