/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;

import net.brokegames.server.Client;

/**
 * @author Steven
 *
 */

public class Player implements Component {

	private int id;
	private String name;
	private Client client;
	private Body body;

	public Player() {}
	
	public Player(Body body) {
		this.body = body;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}
	
	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}
	
	/**
     * @return the body
     */
    public Body getBody() {
      return body;
    }
  
    /**
     */
    public void setBody(Body body) {
      this.body = body;
    }
    
    /**
     * Removes the buddy reference 
     */
    public void removeBody() {
    	this.body = null;
    }
    
}
