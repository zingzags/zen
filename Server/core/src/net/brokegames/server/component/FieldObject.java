package net.brokegames.server.component;

import com.badlogic.ashley.core.Component;

public class FieldObject implements Component {

	private int fieldId;
	
	public FieldObject(int fieldId) {
		this.fieldId = fieldId;
	}
	
	/**
	 * This method will return the field ID for the field object
	 * @return the fieldId
	 */
	public int getFieldId() {
		return fieldId;
	}

	/**
	 * This method will set the unique field ID for the field object
	 * @param fieldId
	 */
	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}
	
	
	public void sendSpawnData() {
	}
	
//	public Vector2 getPosition() {
//		return LocalClient.getPlayer().getBody().getPosition();
//	}	
}
