/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.world;

import com.badlogic.gdx.utils.Array;

/**
 * @author Steven
 *
 */
public class WorldFactory {

	private Array<GameWorld> instances = new Array<GameWorld>();
	
	/**
	 * This class will hold all the {@link GameWorld} instances
	 */
	public WorldFactory() {}
	
	/**
	 * This method adds a world instance to the {@link GameWorld} array
	 * @param {@link GameWorld} world - The new {@link GameWorld} object that will be added
	 */
	public void addWorld(GameWorld world) {
		if (world != null) {
			instances.add(world);
		}
	}
	
	/**
	 * This method will create a new {@link GameWorld} object, then adds it to the {@link GameWorld} array
     */
	public void addWorld(String name) {
		if (!name.isEmpty()) {
			GameWorld world = new GameWorld();
			world.giveWorldName(name);
			instances.add(world);
		}
	}
	
	/**
	 * This method will return the world instance that is specified by the parameter
	 * @return the {@link GameWorld} object
	 */
	public GameWorld getWorld(int index) {
		return instances.get(index);
	}
	
	public void registerHandlers() {
	//	ArrayReflection
	}
	
}
