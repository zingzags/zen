/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.world;

import com.badlogic.gdx.utils.Array;

import net.brokegames.channel.ChannelFactory;

/**
 * @author Steven
 *
 */
public class GameWorld {

	private Array<ChannelFactory> channels = new Array<ChannelFactory>();
	private String worldName;
	
	/**
	 * This class will handle the world logic, It contains 
	 * all the channels{@link ChannelFactory} of the world 
	 */
	public GameWorld() {}
	
	/**
	 * This method will give the world a String.
	 * This String will be the name of the world
	 */
	public void giveWorldName(String worldName) {
		this.worldName = worldName; 
	}
	
	/**
	 * This method returns the name of the world
	 * @return worldName
	 */
	public String getName() {
		return worldName;
	}
	
	/**
	 * This method will add a channel instance to the channels array
	 */
	public void addChannel(ChannelFactory channel) {
		channels.add(channel);
	}
	
	/**
	 * This method will return a channel instance that you have specified with the index
	 */
	public ChannelFactory getChannel(int channel) {
		return channels.get(channel);
	}
	
}
