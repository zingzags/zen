/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.channel;

import net.brokegames.field.MapFactory;
import net.brokegames.field.MapLoader;
import net.brokegames.server.network.ServerInitializer;

/**
 * @author Steven
 *
 */
public class ChannelFactory {

	private int channelId;
	private int port = -1;
	private ServerInitializer channel;
	private MapFactory maps = new MapFactory();
	
	/**
	 * This class will handle the channel that the players will operate on,
	 * The {@link ChannelFactory} class will handle:
	 * 	- Maps
	 * 	- Parties
	 * 	- Creating the physics for a map
	 */	
	public ChannelFactory() {}
	
	/**
	 * This class will handle the channel that the players will operate on,
	 * The {@link ChannelFactory} class will handle:
	 * 	- Maps
	 * 	- Parties
	 * 	- Creating the physics for a map
	 * @param channelId - the Id of the channel
	 */
	public ChannelFactory(int channelId) {
		this.channelId = channelId;
	}
	
	/**
	 * This method adds the port number to the channel
	 * @param port - The port number of the channel
	 */
	public void addPort(int port) {
		this.port = port;
	}
	
	/**
	 * This method will return the port number that is associated with the class
	 * @return int port - The port number of the channel
	 */
	public int getPort() {
		return port;
	}
	
	/**
	 * This method will give the channel an id
	 * @param channelId - The id associated with the channel
	 */
	
	public void giveChannelId(int channelId) {
		this.channelId = channelId;
	}
	
	/**
	 * This method will return the channel id associated with the channel
	 * @return channelId - The channel id
	 */
	public int getChannelId() {
		return channelId;
	}
	
	/**
	 * This method will generate the physics object for all maps
	 */
	public void generateMapPhysics() {
		for (MapLoader map : maps.getMaps()) {
			map.createCollision();
		}
	}

	/**
	 * This method will generate the physics object in the specified map
	 * @param mapId - The id of the map which you want to generate the physics
	 */
	public void generateMapPhysics(int mapId) {
		maps.getMap(mapId).createCollision();
	}
	
	/**
	 * This method will return the specified map
	 * @param mapId - The id of the map which you want to return
	 * @return {@link MapLoader}
	 */
	public MapLoader getMap(int mapId) {
		return maps.getMap(mapId);
	}
	
	/**
	 * This method binds the channel to a port
	 * @param worldId - The Id on which this channel is operating on
	 * @param connections - The amount of connections the port should handle
	 * @return boolean if the channel did manage to bind
	 */
	public boolean bind(int worldId, int connections) {
		if (port == -1) {
			return false;
		}
		channel = new ServerInitializer(port, connections);
		channel.setWorldAndChannel(worldId, channelId);
		if (channel.start()) {
			return true;
		}
		System.out.println("There was an issue trying to bind channel: " + channelId);
		return false;
	}
}
