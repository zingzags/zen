/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.field;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

/**
 * @author Steven
 *
 */
public class MapLoader {

	private String mapName;
	private int mapId;
	private TiledMap tiledMap;
    private LevelCollisionGenerator generator;
    
    /**
     * This class loads the tmx file
     * 
     */
	public MapLoader(String mapName) {
		this.mapName = mapName;
		tiledMap = new TmxMapLoader().load(Gdx.files.internal("maps/" + mapName + ".tmx").toString());
	}

    /**
     * This class loads the tmx file
     * 
     */	
	public MapLoader(int mapId) {
		this.mapId = mapId;
		tiledMap = new TmxMapLoader().load(Gdx.files.internal("maps/" + mapId + ".tmx").toString());
	}	
	
	/**
	 * This method loads the body for the map objects
	 * 
	 */
	public void createCollision() {
		if (tiledMap != null) {
			generator = new LevelCollisionGenerator();
			generator.createPhysics(tiledMap);
		}
	}	
	
	/**
	 * @return the map
	 */
	public String getMapName() {
		return mapName;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(String map) {
		this.mapName = map;
	}

	/**
	 * @return the mapId
	 */
	public int getMapId() {
		return mapId;
	}

	/**
	 * @param mapId the mapId to set
	 */
	public void setMapId(int mapId) {
		this.mapId = mapId;
	}
	
	/**
	 * Destroys the physics in the map
	 */
	public void dispose() {
		if (generator != null) {
			generator.destroyPhysics();
		}
	}
	
	/**
	 * @return the properties of the map
	 */
	public MapProperties getProp() {
		return tiledMap.getProperties();
	}
	
	/**
	 * @return the layers of the map
	 */
	public MapLayers getLayers() {
		return tiledMap.getLayers();
	}
	
}
