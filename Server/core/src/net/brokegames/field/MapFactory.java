/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.field;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

/**
 * @author Steven
 *
 */
public class MapFactory {
	
	private Array<MapLoader> maps = new Array<MapLoader>();
	/**
	 * This class creates the cache for MapCreator,
	 * it stores all the maps in an array
	 */
	public MapFactory() {}
	
	/**
	 * This method loads all the maps inside the
	 * "maps" folder in the local directory.
	 * Then this method adds them to the array
	 */
	public void load() {
		for (FileHandle file : Gdx.files.internal("maps/").list()) {
			MapLoader creator = new MapLoader(file.nameWithoutExtension());
			if (creator != null) {
				maps.add(creator);
			}
		}
	}
	
	/**
	 * This method loads the specified map inside the
	 * "maps" folder in the local directory.
	 * Then this method adds them to the array
	 * @param mapId - The id of the map
	 */
	public void load(int mapId) {
		MapLoader creator = new MapLoader(mapId);
		if (creator != null) {
			maps.add(creator);
		}
	}	
	
	/**
	 *  This method returns the array which all the maps are stored in
	 * @return Array<MapLoader>
	 */
	public Array<MapLoader> getMaps() {
		return maps;
	}
	
	/**
	 * This method adds a map into the array
	 */
	public void addMap(MapLoader map) {
		maps.add(map);
	}
	
	/**
	 * This map returns MapCreator of the map that matches the same mapName
	 * @return MapLoader
	 */
	public MapLoader getMap(String name) {
		for (MapLoader map : maps) {
			if (map.getMapName().equals(name)) {
				return map;
			}
		}
		return null;
	}
	
	/**
	 * This map returns MapCreator of the map that matches the same mapId
	 * @return MapLoader
	 */
	public MapLoader getMap(int id) {
		for (MapLoader map : maps) {
			if (map.getMapId() == id) {
				return map;
			}
		}
		return null;
	}
	
}
