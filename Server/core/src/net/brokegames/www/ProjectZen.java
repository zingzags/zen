package net.brokegames.www;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;

import net.brokegames.field.Field;
import net.brokegames.server.Server;
import net.brokegames.server.ServerConnections;
import net.brokegames.server.ServerConstants;
import net.brokegames.server.network.ServerInitializer;

public class ProjectZen implements ApplicationListener {
	
	private ServerInitializer server;
	private OrthographicCamera camera;
	public static Field field;
	
	@Override
	public void create() {
        ServerConstants.loadConfigurations();
		Gdx.app.setLogLevel(Application.LOG_INFO);
        ServerConnections connection = ServerConstants.config.getConnections()[0];
		server = new ServerInitializer(connection.getPort(), connection.getMaximumConnections());
		server.start();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        new Server();
        field = new Field();
	}

	@Override
	public void render() {
		Gdx.graphics.setTitle(ServerConstants.config.getTitle() + " FPS:" + Gdx.graphics.getFramesPerSecond());
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glEnable(GL30.GL_BLEND);
		Server.master.update(camera);
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}


	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		Server.master.dispose();
		server.close();
	}
	
}
