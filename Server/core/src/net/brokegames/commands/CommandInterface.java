/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.commands;

/**
 * @author Steven
 *
 */
public interface CommandInterface {

	public boolean match();
	public void execute();
	
}
